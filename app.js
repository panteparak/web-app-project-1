var backend = require("./router/backend/backend.js");
var webpages = require('./router/webpages/webpages.js');
var express = require("express");
var cookieParser = require('cookie-parser')
var app = express();

app.use(cookieParser());

// Routes
app.use(express.static(__dirname + '/assets'));
app.use("/api", backend);



var port = 5000;
app.listen(port, function() {
  console.log("Listening on port: " + port);
});
