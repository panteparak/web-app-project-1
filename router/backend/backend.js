var express = require('express');
var multer = require('multer');
var cookiePaarser = require('cookie-parser');
var InstagramAPI = require('../../API/Instagram-API');
var path = require("path");
var upload = multer({ dest: '../../uploads/' });
var router = express.Router();


router.post('/uploadMedia', upload.single('media'), (req, res) => {

	console.log(req.file.buffer);

  // var storage = multer.diskStorage({
  //   destination: function (req, file, cb) {
  //     cb(null, path.join([__dirname, "../../uploads/"]))
  //   },
  //   filename: function (req, file, cb) {
  //     cb(null, file.fieldname + '-' + Date.now())
  //   }
  // })

	res.send("Received")
});


router.post('/login', (req, res) => {

  var username = req.headers.username;
  var password = req.headers.password;

  res.cookie('cookie' + username, username).send('Cookie set for user: ' + username);

  InstagramAPI.login(username, password);
});


router.post('/home', (req, res) => {

  var path = req.headers.path;
  var caption = req.headers.caption;

  /* Retrieve client's cookie for session create */
  var username = req.cookies;
  var session = InstagramAPI.userSession(username);

  InstagramAPI.uploadPhoto(session, path, caption);
});


router.post('/search/:query', (req, res) => {

  var session = req.cookie;
  var targetSoFar = req.params.query;

  InstagramAPI.search(session, targetSoFar);
});


router.post('/follow/:query', (req,res) => {

  var session = req.cookie;
  var target = req.params.query;

  InstagramAPI.follow(session, target);
});


router.post('/unfollow/:query', (req,res) => {

  var session = req.cookie;
  var target = req.params.query;

  InstagramAPI.unfollow(session, target);
});

module.exports = router;
