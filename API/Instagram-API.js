var Client = require('instagram-private-api').V1;
var path = require('path');
var fs = require('fs');

/* This is for testing */
var async = require("async");
/* Ends here */

/* Check whether or not value is null, use for non-required values */
function valueOf(value) {

  var thisValue;

  if (!value) {
    thisValue = null;
  } else {
    thisValue = value;
  }
  return thisValue;
}

/* Create user session, use for function input */
function userSession(username) {

  var device = new Client.Device(username);
  var cookie = new Client.CookieFileStorage(path.join(__dirname, "../cookies", username + ".json"));

  var session = new Client.Session(device, cookie);

  return session;
}

/* Initial login, not for session input */
function login(username, password) {

  if (!password) { return null; }

  var device = new Client.Device(username);
  var cookie = new Client.CookieFileStorage(path.join(__dirname, "../cookies", username + ".json"));

  /* Return user's session */
  return Client.Session.create(device, cookie, username, password);
}


function logout(session) {

  //console.log(session._device);
  var username = session._device.username;

  fs.unlinkSync(path.join(__dirname, "../cookies", username + ".json"));
}


function uploadPhoto(session, path, caption) {

  var thisCaption = valueOf(caption);

  Client.Upload.photo(session, path)
    .then(function(upload) {
      console.log(upload.params.uploadId);
      return Client.Media.configurePhoto(session, upload.params.uploadId, thisCaption);
    })
    .then(function(medium) {
      // configure medium
      console.log(medium.params)
    });

  return true;
}

/* Searching */
function search(session, target, mainCb) {
  async.waterfall([ (cb) => {
    Client.Account.search(session, target).then((result) => {
      cb(null, result)
    })
  }, (result, cb) => {
    var accountList = [];
    var idList = [];
    for (var i = 0; i < result.length; i++){
      idList.push(result[i]._params.id)
      var accountInfo = {
        /* Insert more here if needed */
        "id": result[i]._params.id,
        "username": result[i]._params.username,
        "isFollowing": null,
        "fullname": result[i]._params.fullName,
        "picture": result[i]._params.picture
      }
      accountList.push(accountInfo)
    }
    cb(null, accountList, idList)
  }, (accountList, idList, cb) => {
    Client.Relationship.getMany(session, idList).then((relation) => {
      if (relation.length !== accountList.length) console.log("Array Length is Wrong!");

      for (var i = 0; i < relation.length; i++){
        if (relation[i]._params._id != accountList[i].id) console.log("Something is Wrong!");

        accountList[i].isFollowing = relation[i]._params.following
      }

      cb(null, accountList);
    })
  }
  ], (err, result) => {
    mainCb(err, result);
  });
}

/* Following */
function follow(session, target){
  /* Session is already initialized */
  session
  .then(function(session){
    /* Follow target */
    return [session, Client.Account.searchForUser(session, target)]
  })
  .spread(function(session, account){
    return Client.Relationship.create(session, account.id);
  })
  .then(function(relationship){
    /* Print followed/following list in form of {followedBy: ...., following: ...} */
    console.log(relationship.params);
  })
}

/* Unfollowing */
function unfollow(session, target){
  /* Session is already initialized */
  session
  .then(function(session){
    /* Follow target */
    return [session, Client.Account.searchForUser(session, target)]
  })
  .spread(function(session, account){
    return Client.Relationship.destroy(session, account.id);
  })
  .then(function(relationship){
    /* Print followed/following list in form of {followedBy: ...., following: ...} */
    console.log(relationship.params);
  })
}

// login("peterpanapi", "pa").then((session) => {
//
// }).catch((err) => {
//   console.log("Err: " +err)
// })

module.exports = {
  login: (username, password) => login(username, password),
  logout: (session) => logout(session),
  uploadPhoto: (username, path, caption) => uploadPhoto(session, path, caption),
  userSession: (username) => userSession(username),
  search:  (session, target) => search(session, target),
  follow: (session, target) => follow(session, target),
  unfollow: (session, target) => unfollow(session, target)
};

/* All test below */
/* Test login */
// async.waterfall([
//     function(callback){
//       login("peterpanapi", "iccs340PanTeparak").then((session) => {
//         callback(null, session)
//       })
//     },
//     function(arg, callback){
//       callback(null, logout(arg));
//     }
//     ], function(err, result) {
//     console.log(result);
// })
/* Ends here */

/* Test Follow/Unfollow */
// async.waterfall([
//   function(callback){
//     var thisSession = login("peterpanapi", "iccs340PanTeparak");
//     callback(null, thisSession);
//   },
//   function(arg, callback){
//     unfollow(arg, "instagram");
//   }
//   ], function(err, result) {
//     console.log(result);
//   })
/* Ends here */

/* Test Posting */
// async.waterfall([
//   (cb) => {
//     login("peterpanapi", "iccs340PanTeparak");
//     cb(null);
//   }, (cb) => {
//     var username = "peterpanapi"
//     var session = userSession(username);
//     console.log(session);
//     cb(null, session)
//   }, (session, cb) => {
//     uploadPhoto(session, path.join(__dirname, "../../../../Desktop/", "hqdefault.jpg"), "Hello pan @panteparak");
//     }
//   ],(err, result) => {
//     console.log(result.params);
// })
/* End here */

// async.waterfall([
//   (cb) => {
//     login("peterpanapi", "iccs340PanTeparak");
//     cb(null);
//   }, (cb) => {
//     var username = "peterpanapi"
//     var session = userSession(username);
//     console.log(session);
//     cb(null, session)
//   }, (session, cb) => {
//     cb(null, search(session, "pan"));
//     }
//   ],(err, result) => {
//     console.log(result);
// })

/* Search Test */
// var username = "peterpanapi"
// search(userSession(username), "peter", (err, result) => {
//   console.log(result)
// })
/* Search Test */
/* Ends here */
